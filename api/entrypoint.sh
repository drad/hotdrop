#!/bin/sh
# entrypoint override

echo "- setting file_cleanup cron job..."
echo "${MAINT_CRON_FILE_CLEANUP}" >>/etc/crontabs/root

echo "- setting reporter cron job..."
echo "${MAINT_CRON_REPORTER}" >>/etc/crontabs/root

echo "- starting crond (background), logs are sent to /var/log/cron.log"
crond -l 8 -b -L /var/log/cron.log

echo "- starting app UVICORN_OPTIONS=${UVICORN_OPTIONS}"
#~ gunicorn -k uvicorn.workers.UvicornH11Worker -w ${UVICORN_WORKERS} -b :8000 ${UVICORN_OPTIONS} main:app
uvicorn --host "0.0.0.0" --port 8000 --ws 'none' ${UVICORN_OPTIONS} main:app
