#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

from starlette.testclient import TestClient

from .routes import core_router

client = TestClient(core_router)


def test_healthcheck():
    resp = client.get("/")
    assert resp.status_code == 200, "DANGER: App is unhealthy"
