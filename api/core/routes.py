#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

from datetime import datetime

from config.config import (
    API_DEFAULT_FILE_EXPIRY,
    API_FILE_EXPIRY_MAX_MINUTES,
    API_MAX_FILE_SIZE,
    API_SUPPORT_CONTACT,
    API_SUPPORT_HELP_MESSAGE,
    API_SUPPORT_SUPPORT,
    CORE,
)
from fastapi import APIRouter

core_router = APIRouter()


@core_router.get("/status", status_code=200)
def status():
    """[summary]
    Standard status

    [description]
    Return 200 for health checks.
    """
    return {"status": "OK", "date": datetime.now().strftime("%Y-%m-%d %H:%M:%S")}


@core_router.get("/version", status_code=200)
def version():
    """[summary]
    Application version

    [description]
    Shows application version info
    """
    return {
        "status": "OK",
        "version": CORE["version"],
        "created": CORE["created"],
        "modified": CORE["modified"],
    }


@core_router.get("/help", status_code=200)
def help():
    """[summary]
    Application help

    [description]
    Shows application help info
    """
    return {
        "status": "OK",
        "version": CORE["version"],
        "message": API_SUPPORT_HELP_MESSAGE,
        "contact": API_SUPPORT_CONTACT,
        "support": API_SUPPORT_SUPPORT,
        "upload-max-size-mb": int(API_MAX_FILE_SIZE / 1024 / 1024),
        "upload-default-expiry": API_DEFAULT_FILE_EXPIRY,
        "upload-max-expiry-minutes": API_FILE_EXPIRY_MAX_MINUTES,
    }
