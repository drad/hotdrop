#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

# Check a given directory for total and new files, sending a report of the data to rocketchat.
#   NOTE: this script is designed to be called by cron.

import logging
import os
import sys

import arrow
import graypy
from config.config import (
    API_DATEFORMAT,
    API_RC_CHANNEL,
    API_RC_SERVER_URL,
    API_RC_TOKEN,
    API_RC_USER_ID,
    API_REPORTER_SHOW_DETAIL,
    API_TIMEZONE,
    API_UPLOAD_DIRECTORY,
    DEPLOY_ENV,
    GRAYLOG_HOST,
    GRAYLOG_PORT,
    LOG_LEVEL,
    LOG_TO,
)
from requests import sessions
from rocketchat_API.rocketchat import RocketChat

about = {
    "name": "hotdrop-reporter",
    "version": "2.1.0",
    "modified": "2020-05-22",
    "created": "2020-02-05",
}

logger_base = logging.getLogger("main_logger")
logger_base.setLevel(logging.getLevelName(LOG_LEVEL))
graylog_handler = graypy.GELFUDPHandler(host=GRAYLOG_HOST, port=GRAYLOG_PORT)
console_handler = logging.StreamHandler()
if "graylog" in LOG_TO:
    logger_base.addHandler(graylog_handler)
    pass
if "console" in LOG_TO:
    logger_base.addHandler(console_handler)

logger = logging.LoggerAdapter(
    logging.getLogger("main_logger"),
    {"application_name": about["name"], "application_env": DEPLOY_ENV},
)

if LOG_LEVEL == "DEBUG":
    logger.debug("### App Started With Debug On ###")
logger.warning(
    f"{about['name']} - v.{about['version']} ({about['modified']}) - {LOG_LEVEL}"
)


def main():
    """check a directory (specified in API_UPLOAD_DIRECTORY envvar) and report (to rocketchat) of contents"""

    directory = API_UPLOAD_DIRECTORY
    rpt = f"*Directory Report for {arrow.now(API_TIMEZONE).format(API_DATEFORMAT)}*"
    rpt_detail = ""

    file_count = 0
    file_size = 0
    new_file_count = 0
    new_file_size = 0

    # walk the directory, split all filenames, cast to int, if <= then delete
    for dirName, subdirList, fileList in os.walk(directory):
        for fname in fileList:
            logger.debug(f"\t{fname}")
            ts, fileid = fname.split("_")
            (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime) = os.stat(
                os.path.join(directory, fname)
            )
            created = f"{arrow.get(ctime).to(API_TIMEZONE).format(API_DATEFORMAT)}"
            size = size / 1024 / 1024
            expires = f'{arrow.get(int(ts)).to("US/Eastern").format(API_DATEFORMAT)}'
            # if the file was created today then its is 'new'
            if arrow.get(ctime).to(API_TIMEZONE).format("YYYY-MM-DD") == arrow.now(
                API_TIMEZONE
            ).format("YYYY-MM-DD"):
                new_file_count += 1
                new_file_size += size
            file_count += 1
            file_size += size
            if API_REPORTER_SHOW_DETAIL:
                rpt_detail += f"\n➤ {fname}\n  • size={round(size,1)}mb created={created} expires={expires}"

    rpt += f"\n• Total Files: {file_count} ({round(file_size,1)}mb)"
    rpt += f"\n• New Files: {new_file_count} ({round(new_file_size,1)}mb)"
    if API_REPORTER_SHOW_DETAIL:
        rpt += f"\n―――――――――――{rpt_detail}"

    with sessions.Session() as session:
        rocket = RocketChat(
            user_id=API_RC_USER_ID,
            auth_token=API_RC_TOKEN,
            server_url=API_RC_SERVER_URL,
            session=session,
        )
        rocket.chat_post_message(
            rpt, channel=API_RC_CHANNEL, alias=about["name"]
        ).json()


if __name__ == "__main__":
    sys.exit(main())
