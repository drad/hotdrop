#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

# Check a given directory for expired files and delete if if they exist.
#   NOTE: this script is designed to be called by cron.

import logging
import os
import sys
import time

import graypy
from config.config import (
    API_UPLOAD_DIRECTORY,
    DEPLOY_ENV,
    GRAYLOG_HOST,
    GRAYLOG_PORT,
    LOG_LEVEL,
    LOG_TO,
)

about = {
    "name": "hotdrop-cleaner",
    "version": "2.2.0",
    "modified": "2021-08-26",
    "created": "2020-02-05",
}

logger_base = logging.getLogger("main_logger")
logger_base.setLevel(logging.getLevelName(LOG_LEVEL))
graylog_handler = graypy.GELFUDPHandler(host=GRAYLOG_HOST, port=GRAYLOG_PORT)
console_handler = logging.StreamHandler()
if "graylog" in LOG_TO:
    logger_base.addHandler(graylog_handler)
    pass
if "console" in LOG_TO:
    logger_base.addHandler(console_handler)

logger = logging.LoggerAdapter(
    logging.getLogger("main_logger"),
    {"application_name": about["name"], "application_env": DEPLOY_ENV},
)

if LOG_LEVEL == "DEBUG":
    logger.debug("### App Started With Debug On ###")
logger.warning(
    f"{about['name']} - v.{about['version']} ({about['modified']}) - {LOG_LEVEL}"
)


def main():
    """Clean up files in a directory. Any file that has a timestamp less than the current timestamp will be deleted.
    Note: the 'directory' which will be cleaned is specified by the API_UPLOAD_DIRECTORY envvar.
    """

    current = time.time()
    directory = API_UPLOAD_DIRECTORY

    logger.debug(f"- directory: {directory} - current timestamp: {current}")

    # walk the directory, split all filenames, cast to int, if <= then delete
    for dirName, subdirList, fileList in os.walk(directory):
        for fname in fileList:
            logger.debug(f"\t- file: {fname}")
            ts, fileid = fname.split("_")
            logger.debug(f"\t- ts: {ts}, fileid: {fileid}")
            if float(ts) < current:
                logger.info(f"\t- file expired, delete: {fname}")
                os.remove(os.path.join(directory, fname))
            else:
                logger.debug(f"\t- file not past expiry: {fname}")


if __name__ == "__main__":
    sys.exit(main())
