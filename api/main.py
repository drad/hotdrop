#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging

import graypy
from config.config import (
    API_BASE_PATH,
    API_RC_ENABLED,
    API_VERSION,
    CORE,
    DEPLOY_ENV,
    GRAYLOG_HOST,
    GRAYLOG_PORT,
    LOG_LEVEL,
    LOG_TO,
)
from core.routes import core_router
from drops.routes import drops_router
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

api_path = f"{API_BASE_PATH}/{API_VERSION}"

logger_base = logging.getLogger("default")
logger_base.setLevel(logging.getLevelName(LOG_LEVEL))
graylog_handler = graypy.GELFUDPHandler(host=GRAYLOG_HOST, port=GRAYLOG_PORT)
console_handler = logging.StreamHandler()
if "graylog" in LOG_TO:
    logger_base.addHandler(graylog_handler)
if "console" in LOG_TO:
    logger_base.addHandler(console_handler)

logger = logging.LoggerAdapter(
    logging.getLogger("default"),
    {"application_name": CORE["name"], "application_env": DEPLOY_ENV},
)

logger.info(
    f"{CORE['name']} - v.{CORE['version']} ({CORE['modified']}) - {LOG_LEVEL} - {LOG_TO}"
)
logger.debug(f"- Base Path:  {api_path}")
logger.debug(f"- RC Notify:  {API_RC_ENABLED}")

app = FastAPI(
    title=f"{CORE['name']}",
    description=f"{CORE['description']}",
    version=f"{CORE['version']}",
    openapi_url=f"{api_path}/openapi.json",
    docs_url=f"{api_path}/docs",
    redoc_url=None,
)
app.add_middleware(
    CORSMiddleware, allow_origins=["*"], allow_methods=["*"], allow_headers=["*"]
)

app.include_router(
    drops_router,
    prefix=f"{api_path}/drops",
    tags=["drops"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    core_router,
    prefix=f"{API_BASE_PATH}",
    tags=["core"],
    responses={404: {"description": "Not found"}},
)


@app.on_event("startup")
async def app_startup():
    """Do tasks related to app initialization."""
    pass


@app.on_event("shutdown")
async def app_shutdown():
    """Do tasks related to app termination."""
    pass
