#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging

import arrow
from config.config import (
    API_DEFAULT_FILE_EXPIRY,
    API_FILE_EXPIRY_MAX_MINUTES,
    API_RC_CHANNEL,
    API_RC_SERVER_URL,
    API_RC_TOKEN,
    API_RC_USER_ID,
    CORE,
)
from requests import sessions
from rocketchat_API.rocketchat import RocketChat

logger = logging.getLogger("default")


def fix_expiry(in_expiry: str):
    """Check to ensure a valid expiry and converts the expiry (string) to a timestamp.
    Supported Suffixes: w (week), m (minute), d (day), and h (hour)
    """
    logger.debug(f"- expiry to timestamp=[{in_expiry}]")
    if not in_expiry:
        expiry = API_DEFAULT_FILE_EXPIRY
    else:
        expiry = in_expiry

    # get time amount
    amount = int(expiry[:-1])
    # convert everything to minutes.
    amount_modifier = 1
    if expiry.endswith("w"):
        amount_modifier = 7 * 24 * 60
    elif expiry.endswith("d"):
        amount_modifier = 24 * 60
    elif expiry.endswith("h"):
        amount_modifier = 60
    elif expiry.endswith("m"):
        pass
    else:
        logger.error(
            f"Unknown expiry designator [{expiry[-1:]}], cannot calculate timestamp."
        )

    adj = amount * amount_modifier
    if adj > API_FILE_EXPIRY_MAX_MINUTES:
        logger.WARNING(
            "- file expiry max hit, expiry has been changed to use API_FILE_EXPIRY_MAX_MINUTES"
        )
        adj = API_FILE_EXPIRY_MAX_MINUTES

    return arrow.get(arrow.utcnow()).shift(minutes=+adj).timestamp()


def notify_of_upload(file_id: str, file_size: str, expiry: str):
    """Notify of upload."""

    try:
        with sessions.Session() as session:
            rocket = RocketChat(
                user_id=API_RC_USER_ID,
                auth_token=API_RC_TOKEN,
                server_url=API_RC_SERVER_URL,
                session=session,
            )
            rocket.chat_post_message(
                f"*New Upload*\n file: {file_id}\n• size: {file_size}\n• expiry: {expiry}",
                channel=API_RC_CHANNEL,
                alias=CORE["name"],
            ).json()

        logger.debug(
            f"- uploaded file ({file_id}) has size of: {file_size} and expiry of: {expiry}"
        )
    except Exception as e:
        logger.error(f"NOTIFY ERROR: could not notify because: {e}")
