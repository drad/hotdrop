#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import tomllib
from os import getenv


def _env_strtobool(val):
    if val in ["true", "'true'"]:
        return True
    return False


def load_config() -> dict:
    """load config data from toml config file"""
    conf = {}
    try:
        with open("config/config.toml", "rb") as f:
            conf = tomllib.load(f)
    except FileNotFoundError as e:
        print(f"File not found: {e}")
    except Exception as e:
        print(f"Other Exception: {e}")
    return conf


_base = load_config()
# top-level/special components of config (for quicker access)
API = _base["api"]
CORE = _base["core"]

API_BASE_PATH = getenv("API_BASE_PATH", "/api")
API_VERSION = API["versions"]["current"]

API_SUPPORT_CONTACT = getenv("API_SUPPORT_CONTACT", "")
API_SUPPORT_SUPPORT = getenv("API_SUPPORT_SUPPORT", "")
API_SUPPORT_HELP_MESSAGE = getenv("API_SUPPORT_HELP_MESSAGE", "Application Help")

API_REPORTER_SHOW_DETAIL = getenv("API_REPORTER_SHOW_DETAIL", "false")
# API_MAX_FILE_SIZE is in bytes
API_MAX_FILE_SIZE = int(getenv("API_MAX_FILE_SIZE_MB", "16")) * 1024 * 1024

API_DEFAULT_FILE_EXPIRY = getenv("API_DEFAULT_FILE_EXPIRY", "1d")
API_FILE_EXPIRY_MAX_MINUTES = int(getenv("API_FILE_EXPIRY_MAX_MINUTES", "43200"))

API_TIMEZONE = getenv("API_TIMEZONE", "US/Eastern")
API_DATEFORMAT = getenv("API_DATEFORMAT", "YYYY-MM-DD HH:mm:ss ZZ")

API_EXTERNAL_URL = getenv("API_EXTERNAL_URL", "http://localhost:8006")

API_UPLOAD_DIRECTORY = getenv("API_UPLOAD_DIRECTORY", "/uploads")

API_RC_ENABLED = _env_strtobool(getenv("API_RC_ENABLED", "false"))
API_RC_SERVER_URL = getenv("API_RC_SERVER_URL", "http://localhost")
API_RC_USER_ID = getenv("API_RC_USER_ID", "guest")
API_RC_TOKEN = getenv("API_RC_TOKEN", "guest")
API_RC_CHANNEL = getenv("API_RC_CHANNEL", "hotdrop")

# ENV Based Variables
DEPLOY_ENV = getenv("DEPLOY_ENV", "prd")

# logging
LOG_TO = getenv("LOG_TO", "console").split(",")
LOG_LEVEL = getenv("LOG_LEVEL", "DEBUG")
GRAYLOG_HOST = getenv("GRAYLOG_HOST", None)
GRAYLOG_PORT = int(getenv("GRAYLOG_PORT", "12201"))
