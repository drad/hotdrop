#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

# import pytest
from .config import load_config


def test_config():
    conf = load_config()
    conf_values = [
        "HOST",
        "PORT",
        "NAME",
    ]
    for value in conf_values:
        assert (
            bool(conf.get("databases", {}).get("default", {}).get(value, "")) is True
        ), f"{value}, not found on config.yml"
