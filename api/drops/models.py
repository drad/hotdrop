#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

from pydantic import BaseModel


class DropUpResponse(BaseModel):
    """Used to abstract out basic fields\n
    Arguments:
        BaseModel {[type]} -- [description]
    """

    status: str = None
    key: str = None
    link: str = None
    expires: str = None
