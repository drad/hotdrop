#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging
from os import path
from uuid import uuid4

import arrow
from config.config import (
    API_BASE_PATH,
    API_DATEFORMAT,
    API_DEFAULT_FILE_EXPIRY,
    API_EXTERNAL_URL,
    API_MAX_FILE_SIZE,
    API_RC_ENABLED,
    API_TIMEZONE,
    API_UPLOAD_DIRECTORY,
    API_VERSION,
)
from fastapi import APIRouter, File, HTTPException, Response, UploadFile, status
from fastapi.responses import FileResponse
from utils import fix_expiry, notify_of_upload

from .models import DropUpResponse

logger = logging.getLogger("default")
api_path = f"{API_BASE_PATH}/{API_VERSION}"

drops_router = APIRouter()


@drops_router.get(
    "/",
    responses={
        200: {
            "content": {"application/octet-stream": {}},
            "description": "Return the file requested.",
        },
        404: {"content": {"application/json": {}}, "description": "File not found."},
    },
)
async def get_file(file_id: str, response: Response):
    """[summary]
    Get a file by file_id.

    [description]
    Endpoint to get a file by file_id.
    """
    logger.debug(f"- getting file: {file_id}")
    f = path.join(API_UPLOAD_DIRECTORY, file_id)
    if path.exists(f):
        logger.debug("- that file exists, here ya go!")
        return FileResponse(f)
    else:
        logger.error(f"File does not exist: {f}")
        response.status_code = status.HTTP_404_NOT_FOUND
        return response


@drops_router.post("/", response_model=DropUpResponse)
async def create_upload_file(
    file: UploadFile = File(...), expiry: str = API_DEFAULT_FILE_EXPIRY
):
    """[summary]
    Uploads a file.

    [description]
    Endpoint to upload a file with option for expiry.
    """
    logger.debug(
        f"- received file upload: {file.filename} with expiry: {expiry} content-type: {file.content_type}"
    )
    contents = await file.read()
    logger.debug(
        f"  - file size is: {len(contents)} and API_MAX_FILE_SIZE={API_MAX_FILE_SIZE}"
    )

    # check file size.
    if len(contents) < API_MAX_FILE_SIZE:
        logger.debug(f"- file size is ok: {len(contents)}")
    else:
        logger.debug(
            f"File too big ({len(contents)}) while API_MAX_FILE_SIZE is {API_MAX_FILE_SIZE}."
        )
        raise HTTPException(status_code=413)

    # NOTE: expiry will be changed to API_FILE_EXPIRY_MAX_MINUTES if
    #   expiry > API_FILE_EXPIRY_MAX_MINUTES
    ts = fix_expiry(expiry)
    logger.debug(f"- ts is: [{ts}]")
    expires_at = arrow.get(ts).to(API_TIMEZONE).format(API_DATEFORMAT)
    logger.debug(f"- file expiry timestamp is: {ts} ({expires_at})")
    fileid = f"{ts}_{uuid4().hex}"
    logger.debug(f"- fileid is: {fileid}, saving to: {API_UPLOAD_DIRECTORY}")
    with open(path.join(API_UPLOAD_DIRECTORY, fileid), "wb") as w:
        w.write(contents)
    file_size_mb: str = f"{round(len(contents) / 1024 / 1024, 1)}mb"
    expiry_str: str = f"{expires_at} ({expiry})"
    if API_RC_ENABLED:
        notify_of_upload(fileid, file_size_mb, expiry_str)

    return DropUpResponse(
        status="OK",
        key=fileid,
        link=f"{API_EXTERNAL_URL}{api_path}/drops/?file_id={fileid}",
        expires=expires_at,
    )
