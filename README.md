# README

a cloud-native, light, anonymous file sharing service.


##  Features  ##

- no database
- logging to console and/or graylog
- easily configurable (change timezone, date format, default expiry, allowed upload types, upload size, and more)
- notifications to rocketchat (if you like)
- runs in kubernetes or docker (docker-compose file included)


##  Usage  ##

```
server=localhost
port=:8006
base_path=/api/v2
```

###  Help  ###
- with httpie: `http ${server}${port}${base_path}/help`
    - or use the swagger ui by going to your server URL + Base Path + /docs (e.g. http://localhost:8006/api/v2/docs)


###  Uploading  ###

- with httpie: `http --form POST ${server}${port}${base_path} "file@file1.png"`
    - with expiry: `http --form POST ${server}${port}${base_path}?expiry=1h "file@file1.png"`
    - example: `http --form POST http://localhost:8006/api/v2/drops/?expiry=5m "file@test-1.jpg"`


###  Downloading  ###

- with wget: `wget -nv -O my-file.png <link-shown-from-upload-response>`
    - example: `wget -nv -O test-1.jpg "http://localhost:8006/api/v2/drops/?file_id=1590231128_8d9f1c640c2d465aad873f3325a3b9f4"`
